# modernization-scenarios

[![Netlify Status](https://api.netlify.com/api/v1/badges/def53362-9ebf-42ad-92ef-b480cbdd1d45/deploy-status)](https://app.netlify.com/sites/modernization-scenarios/deploys)

This website is available in these locations:

https://modernization-scenarios.netlify.app/



## Description
This is a set of references for scenarios that I have come across while working with teams modernising their application stack. It's not intended to be a `reference architecture` but a set of topics that descibe common issues that developers come across when moving mature applications to containers. 

## Installation
This is a static website that uses mdbook.
To generate the site run `./mdbook build`
This will create a static site in a folder called `book`

## Support
Please open an issue if you have a question.

## Roadmap
Roadmap is adhoc as this are collected during conversations I have.

## Contributing
Useful patterns are always welcome. Make sure code assets are as minimal as possible and have clear instructions on thier usage. 

## Authors and acknowledgment
Anton Whalley (@No9)

## License
MIT

## Project status
Currently being worked on.