# Streamline Builds

Ensure only the required assets are deployed in a container image.

## Context and problem

In order to make images as small as possible and minimise the impact of security risks it's important to use a minimal base image that you build up with capabilities.


## Solution

Use the container builder pattern

This scenario consists of a Dockerfile that builds a Rust project with external SSL dependancies and the configures a minimal image to use it.

A working reference is available in the [repo for this project](https://gitlab.com/no9/modernization-scenarios/-/tree/main/scenarios/streamline-builds).


## Issues and considerations

While a single base image is the desired goal it may be necessery to use more than one base image.

Ensure security scans of the images and a process to manage them is in place.

Understand how you are going to support the OS layer and related system libraries.

## When to use this scenario

As you move into the Replatform stage of modernization it is good practice to put this scenario in place.

## Example

```bash
{{#include Dockerfile}}
```

## Related resources

[Standard Operating Environment](/standard-operating-environment/)

