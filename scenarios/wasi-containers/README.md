# WASI Containers

End to end demonstration of running [WASI](https://wasi.dev/) in a [OCI](https://opencontainers.org/) compliant container runtime using crun on fedora 37

## Context and problem

In a resource constrained environments such as edge locations or in sustainability focused organisations the benefits of containers need to be refined to make the images as small as possible, improve startup times and minimise the energy used for execution.

## Solution

Use a minimal container runtime environment written in a high performance language that only requires a *scratch* based image but still supports multiple languages.

## Issues and considerations

WASI is still a maturing standard with networking and device integration still in the early stages of development.

## When to use this scenario

Use this scenario when you have environments with limited resources such as edge or when you need fast container start up times.

## Example

See the folder https://gitlab.com/no9/modernization-scenarios/-/tree/main/scenarios/wasi-containers for the full source code.

## prereqs

First you need to install podman wasmedge and buildah.

```
sudo dnf install podman wasmedge buildah
```

Then you need to build and install crun a minimal container runtime with [WASMEdge](https://wasmedge.org/) support.

```
git clone https://github.com/containers/crun.git
cd crun/
./autogen.sh
./configure --with-wasmedge
make

mv /usr/bin/crun /usr/bin/curn.backup 
sudo mv /usr/bin/crun /usr/bin/curn.backup 
sudo mv crun /usr/bin
```

Now install rust with the WASI target. 

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup target add wasm32-wasi
```

## create the source files

Create a new folder called `wasi-containers` 

```bash
mkdir wasi-containers
```

Add the crate configuration

```toml
{{#include Cargo.toml}}
```


Add a `src` folder and create a simple program that prints the environment variables.

```rust
{{#include src/main.rs}}
```

Finally create a Dockerfile in the root folder of the project which copies the wasm file into an empty container.

```bash
{{#include Dockerfile}}
```

## build 

Build the Dockerimage 

```
cargo build --release --target wasm32-wasi
buildah build --annotation "module.wasm.image/variant=compat" -t mywasm-image
```

## run 

```
podman run -e LIFE="GOOD" mywasm-image:latest
```

### outputs
```
Hello, world!
PATH: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
TERM: xterm
container: podman
LIFE: GOOD
HOSTNAME: ad0c6217d785
HOME: /
```

## Related resources

[WASI workloads on Podman](https://opensource.com/article/22/10/wasm-containers)

[Ansible roles with Podman](https://fedoramagazine.org/automate-container-management-on-fedora-linux-with-the-podman-linux-system-role/)

[Standalone Podman](/standalone-podman/)
