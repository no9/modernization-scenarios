# Microtemplates with Helm

## Context and problem

The ability to configure containers with external definitions is a key part of [12 factor apps](https://12factor.net/).
As organisations adopt containers these external definitions tend to travel as shell scripts included in the code repository.
These shell scripts become unwieldy quickly and have to be rewritten when an organisation moves to an orchestration platform.

## Solution

Adopt a template engine early in the modernization cycle can help mitigate the problem of shell scripts and prepare an organisation for the move towards kubernetes.

## Issues and considerations

Helm as a dependancy may require additional training for the organisation.

## When to use this scenario 

Helm is useful from Replatform point of the journey onwards. This microtemplating option is targeting the developer loop and is especially useful at the start of the Replatform roadmap.

## Example

1. Prerequisits

    - [Helm](https://helm.sh/docs/intro/install/) 
    - [Podman](https://podman.io/getting-started/installation)

1. Create a file called `Chart.yaml file and add the following content

```yaml
{{#include Chart.yaml}}
```

3. Create folder called `templates` and create file called `pod.yaml` with the following content 

```yaml
{{#include templates/pod.yaml}}
```

4. Create a file called `values.yaml` with the following content

```yaml
{{#include values.yaml }}
```

5. Validate the pod template is generated correctly by running `helm template .`

    ```
    helm template .

    ---
    # Source: top-helm-app/templates/pod.yaml
    apiVersion: v1
    kind: Pod
    metadata:
    name: top-helm-pod
    spec:
    containers:
        command:
        - top
        name: top-helm-container
        image: "docker.io/library/alpine:3.15.4"

    ```

## Next steps
1. Run the file with podman 

    ```
    helm template . | podman play kube -
    ```

1. Delete the pod 

    ```
    podman pod stop top-helm-top-pod 
    podman pod rm top-helm-top-pod 

    ```
## Related resources

[Standalone Podman](/standalone-podman/)
