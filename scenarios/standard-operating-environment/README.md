# Standard Operating Environment

## Context and problem

A common anti-pattern is to use a distribution from open source projects as the base image for specific functionality. This can be managable in small deployments but as the amount of deployed containers grow it will get problematic from a dependency and security management perspective. 

Take for example the following deployment topology.

![non-standard operating environment](./soe.png)

For example in the above scenario you will have

- 6 Different versions of glibc
- 2 Different versions of muscl
- 8 Different versions of OpenSSL

## Solution

The recommendation here is to standardize on a base distribution image so that managing CVEs and building generic tooling for debugging is simpler.

My personal preference is to use [Red Hat Universal base images](https://developers.redhat.com/products/rhel/ubi) but an experienced team could also target [alpine](https://hub.docker.com/_/alpine) as it has an opinionated security posture that some teams my find useful as well as a [distroless approach](https://www.chainguard.dev/unchained/introducing-apko-bringing-distroless-nirvana-to-alpine-linux).

## Issues and considerations

Understand how you are going to get support either internally or externally for the base images.

Be concious about the overhead of 

## When to use this scenario 

A standard operating environment initiative should be embarked on when starting the Replatform phase.
As the number of base operating systems types grows beyond 5 consider standardising.

## Example

As well as targeting a single distribution from a vendor organisations may also want to build a base image internal to their organisation with the following benefits:

- Userspace libraries, runtimes etc are consistent
- Improved inner dev loop with shorter docker builds
- Easier to update and integrate with *build on change* processes

See the Dockerfiles in the [scenario folder](https://gitlab.com/no9/modernization-scenarios/-/tree/main/scenarios/standard-operating-environment) for a reference implementation.

[orgbase.Dockerfile](./orgbase.Dockerfile) is the organisational base image.
In this example the base image is built with 
`podman build -t quay.io/mod_scenarios/orgbase -f orgbase.Dockerfile`

```bash
{{#include orgbase.Dockerfile}}
```
[app.Dockerfile](./app.Dockerfile) is the image that will be deployed. 
```bash
{{#include app.Dockerfile}}
```

## Related resources

[Streamline Builds](/streamline-builds/)