# Use the organisation base image
FROM quay.io/mod_scenarios/orgbase

# this is just a simple example but you may want to integrate 
# this orgbase image approach with the streamline-builds scenario  
ENTRYPOINT [ "while true; do ls /root/buildinfo/; sleep 30; done" ]