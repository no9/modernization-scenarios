# Create an organisational base image from a vendor release.
FROM registry.access.redhat.com/ubi8/ubi-minimal

# Update it with a specific configuration.
# ubi-minimal uses `microdnf` a small pkg management system.
RUN  microdnf update && microdnf install -y procps-ng