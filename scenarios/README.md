# Overview

This site is a set of topics that teams may find useful while modernising an application from virtual machines into containers.

The goal is not to provide a [reference architecture](https://www.ibm.com/cloud/architecture/architectures/cloud-native/reference-architecture/) but to compile a set of real world topics that developers may find when moving mature applications to containers.

The scenarios are likely to be most relevent to the Replatform and Repackage steps illustrated in the diagram below.

These two steps are the grey areas of modernization that are usually very specific to the application and organisation being modernized and where general guidance isn't clearly mapped by the standard litrature.

![Cloud Native Journey](./journey.png)
