# Standalone Podman

## Context and problem

The first step a lot of organizations take when moving from virtual servers to containers is to run a single container within a virtual server instance using a container runtime such as docker or podman.

This may seem an unneccessery overhead it provides a path for the software development team to use containers in dev/test and in the continuous integration process while not putting to much burden on the operations organisation.

While it's not an ideal scenario as the orchestration tooling around containers is one of the key benefits it does enable an organisation to break up the modernization journey into achievable goals and doesn't require a larger organisational transformation.

## Solution

The selection of a container management system is dependant on whether an organisation intends to eventually adopt kubernetes or not.
If kubernetes is an end goal then it makes sense to target podman as it has built in support for defining collections of containers in pods in exactly the same way as kubernetes and this will help smooth the transition later in the journey.

## Issues and considerations

Organisations should be aware that most of the time this is an interim step and should be concious not to start building out a custom container orchestration system when moving to kubernetes would be the better option. 

Also consider the refactoring required for observability and CI/CD if this is a substantial peice of work it may make sense to target kubernetes directly or at least ensure the changes align with the target architecture in the Refactor step.

Managing pods with systemd and podman is an evolving space so expect some changes as podman evolves.

The `podman generate systemd --files --name` approach had a some rough edgeds and configuring a system that could survive a reboot was **never** achieved with this approach. It is strongly advised to use the `systemctl --user start podman-kube` given below unless there are some specific circumstanses that prevents this.

## When to use this scenario 

In the early stages of moving from Rehost to Replatform

## Example

### Prerequisits

- Systemd compatible distribution
- [Podman](https://podman.io/getting-started/installation)

### Test on

- Ubuntu 22.04 - podman 4.3.0

- Fedora 36 - podman 4.2.1

### Steps

1. Create a file called `top.yaml` and add the following content

```yaml
{{#include top.yaml}}
```

2. Create the escaped configuration string

    ```
    escaped=$(systemd-escape $PWD/top.yaml)
    ```
1. Run the yaml file as a systemd service

    ```
    systemctl --user start podman-kube@$escaped.service
    ```

1. Generate the service config from the running instance to persist reboots.

    ```
    systemctl --user enable podman-kube@$escaped.service
    ```

1. Reboot you machine to confirm the service survives reboots.

## Additional Notes

The systemd user instance is killed after the last session for the user is closed. The systemd user instance can be started at boot and kept running even after the user logs out by enabling lingering using

```
loginctl enable-linger <username>
```
## Related resources

[Red Hat Podman Systemd](https://www.redhat.com/sysadmin/kubernetes-workloads-podman-systemd)

[Tutorial Works](https://www.tutorialworks.com/podman-systemd/)

[Podman Documentation](https://docs.podman.io/en/latest/markdown/podman-generate-systemd.1.html)

