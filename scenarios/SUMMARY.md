# Summary

- [Overview](README.md)
- [Standard Operating Environment](standard-operating-environment/README.md)
- [Streamline Builds](streamline-builds/README.md)
- [Dependency Injection](dependency-injection/README.md)
- [Standalone Podman](standalone-podman/README.md)
- [Helm Mico-tempates](helm-micro-templates/README.md)
- [WASI Containers](wasi-containers/README.md)
