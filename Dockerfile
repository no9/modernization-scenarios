FROM registry.access.redhat.com/ubi8/ubi

RUN yum install -y gcc openssl-devel && \
    rm -rf /var/cache/dnf && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

ENV PATH=/root/.cargo/bin:${PATH}

RUN cargo install mdbook